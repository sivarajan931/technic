Technic
=======


Digiline on Machines
-------

Some **machines** support some digiline event and digiline command:
* electric furnace
* compressor
* compactor
* extractor
* alloying furnace
* centrifuge
* sawmill
* thresher
  
#### Machines Digiline Event:
when you add a channel it activates digiline event on those machines:
  ###### Work Event
this event is thrown when :
- the machine is **Starting** to process some material after idle time. You will receive a table like this : `{type="auto", status = "Starting"}`
- the machine is **Ending** to process some material and is changing to idle state. You will receive a table like this : `{type="auto", status = "Ending"}`
- the machine **Stalled** (output inventory is full) : `{type="auto", status = "Stalled"}`
- the machine is **Restarting** after being stalled : `{type="auto", status = "Restarting"}`


#### Machines Digiline command:
   ###### "state" command
   syntax: <state> 
   example: `digiline_send("myChannel","state")`
   sending the "state" digiline command, you will recieve informations about you machine state, it's a message with those informations :    
   ```lua
{ 
  {name ="my:nodename",max=99,count=75},
  {name ="my:nodename",max=99,count=75},
  ....  --- as much as input source is there
  type ="status",
  state="Active"|"Idle"|"Unpowered"|"Stalled", 

}
```

**Example:**
```lua 
-- Example 1: sending commande with digiline :
 digiline_send("myChannel","state")
 ```
 ```lua
--[[you will recieve a table like this:
{
	{
		max = 99,
		count = 60,
		name = "default:steel_ingot"
	},
	{
		max = 99,
		count = 60,
		name = "technic:coal_dust"
	},
	type = "status",
	state = "Active"
}
]]
--}


-- Example 2 use digiline response in a lua tube/controller:
if event.type == "digiline" then
  -- in a furnace only one slot in source inventory.
  local progress = event.msg[1].count
  local max = event.msg[1].max
  local percent = 100.0 * progress / max 
end
```
   ###### "eject_input" command
   synthax: eject_input [top|bottom|right|left|front|back]
   example: `digiline_send("myChannel","eject_input back")`
   the eject command allow you to eject remaining material in the source list (material that are not processed yet). 

  -  _top is default direction: sending only "eject_input" is like sending "eject_input top"_
  -  if a tube is connected on the choosen direction, the stack is injected , else the stack is drop.



Credits
-------
Credits for contributing to the project (in alphabetical order):
  * kpoppel
  * Nekogloop
  * Nore/Ekdohibs
  * ShadowNinja
  * VanessaE
  * And many others...

FAQ
---

1. My technic circuit doesn't work.  No power is distrubuted.
  * A: Make sure you have a switching station connected.

License
-------

Unless otherwise stated, all components of this modpack are licensed under the 
LGPL, V2 or later.  See also the individual mod folders for their
secondary/alternate licenses, if any.
